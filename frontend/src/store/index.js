import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';


Vue.use(Vuex)

//Variables que se usaran para hacer persistentes en el sistema usando el vuex-store
const getDefaultState = () => {
  return {
    token: '',
    id: '',
    p: [],
    idProceso: '',
    usuarios: [],
    superadmin: '',
    isResProceso: '',
    isRespPortCurso: '',
    isColabPortCurso: '',
    isRespCursoAct: '',
    fechaActual: '',
    fechaInicio: '',
    fechaFinFase1: '',
    fechaFinFase2: '',
    fechaFinFase3: '',
    fechaFinFase4: '',
  };
};

//Aca se manejan el state (que son las variables que declare arriba para que se hagan persistente)
//luego usamos getters que son metodos para obtener las variables declaradas
//el mutations donde se llenan datos en las variables
//actiones donde se llama al mutatios a ejecutar desde codigo mismo 
export default new Vuex.Store({
  state: getDefaultState(),
  getters: {
    isLoggedIn: state => {
      return state.token;
    },
    getId: state => {
      return state.id;
    },
    idProceso: state => {
      return state.idProceso;
    },
    getPermisos: state => {
      return state.p;
    },
    isSuperadmin: state => {
      return state.superadmin;
    },
    isResProceso: state => {
      return state.isResProceso;
    },
    isRespPortCurso: state => {
      return state.isRespPortCurso;
    },
    isColabPortCurso: state => {
      return state.isColabPortCurso;
    },
    isRespCursoAct: state => {
      return state.isRespCursoAct;
    },
    getFechaActual: state => {
      return state.fechaActual;
    },
    getFechaInicio: state => {
      return state.fechaInicio;
    },
    getFechaFinFase1: state => {
      return state.fechaFinFase1;
    },
    getFechaFinFase2: state => {
      return state.fechaFinFase2;
    },
    getFechaFinFase3: state => {
      return state.fechaFinFase3;
    },
    getFechaFinFase4: state => {
      return state.fechaFinFase4;
    },
    getUsuarios: state => {
      return state.usuarios;
    }
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_ID: (state, id) => {
      state.id = id;
    },
    SET_IDPROCESO: (state, idProceso) => {
      state.idProceso = idProceso;
    },
    SET_PERMISOS: (state, p) => {
      state.p = p;
    },
    SET_SUPERADMIN: (state, superadmin) => {
      state.superadmin = superadmin;
    },
    SET_ISRESPPROCESO: (state, isResProceso) => {
      state.isResProceso = isResProceso;
    },
    SET_ISRESPPORTCURSO: (state, isRespPortCurso) => {
      state.isRespPortCurso = isRespPortCurso;
    },
    SET_ISCOLABPORTCURSO: (state, isColabPortCurso) => {
      state.isColabPortCurso = isColabPortCurso;
    },
    SET_ISRESPCURSOACT: (state, isRespCursoAct) => {
      state.isRespCursoAct = isRespCursoAct;
    },
    SET_FECHAACTUAL: (state, fechaActual) => {
      state.fechaActual = fechaActual;
    },
    SET_FECHAINICIO: (state, fechaInicio) => {
      state.fechaInicio = fechaInicio;
    },
    SET_FECHAFINFASE1: (state, fechaFinFase1) => {
      state.fechaFinFase1 = fechaFinFase1;
    },
    SET_FECHAFINFASE2: (state, fechaFinFase2) => {
      state.fechaFinFase2 = fechaFinFase2;
    },
    SET_FECHAFINFASE3: (state, fechaFinFase3) => {
      state.fechaFinFase3 = fechaFinFase3;
    },
    SET_FECHAFINFASE4: (state, fechaFinFase4) => {
      state.fechaFinFase4 = fechaFinFase4;
    },
    SET_USUARIOS: (state, usuarios) => {
      state.usuarios = usuarios;
    },
    RESET: state => {
      Object.assign(state, getDefaultState());
    }
  },
  actions: {
    login: ({ commit }, { token }) => {
      commit('SET_TOKEN', token);
    },
    userId: ({ commit }, { id }) => {
      commit('SET_ID', id);
    },
    idProceso: ({ commit }, { idProceso }) => {
      commit('SET_IDPROCESO', idProceso);
    },
    permisos: ({ commit }, { p }) => {
      commit('SET_PERMISOS', p);
    },
    superadmin: ({ commit }, { superadmin }) => {
      commit('SET_SUPERADMIN', superadmin);
    },
    isResProceso: ({ commit }, { isResProceso }) => {
      commit('SET_ISRESPPROCESO', isResProceso);
    },
    isRespPortCurso: ({ commit }, { isRespPortCurso }) => {
      commit('SET_ISRESPPORTCURSO', isRespPortCurso);
    },
    isColabPortCurso: ({ commit }, { isColabPortCurso }) => {
      commit('SET_ISCOLABPORTCURSO', isColabPortCurso);
    },
    isRespCursoAct: ({ commit }, { isRespCursoAct }) => {
      commit('SET_ISRESPCURSOACT', isRespCursoAct);
    },
    fechaActual: ({ commit }, { fechaActual }) => {
      commit('SET_FECHAACTUAL', fechaActual);
    },
    fechaInicio: ({ commit }, { fechaInicio }) => {
      commit('SET_FECHAINICIO', fechaInicio);
    },
    fechaFinFase1: ({ commit }, { fechaFinFase1 }) => {
      commit('SET_FECHAFINFASE1', fechaFinFase1);
    },
    fechaFinFase2: ({ commit }, { fechaFinFase2 }) => {
      commit('SET_FECHAFINFASE2', fechaFinFase2);
    },
    fechaFinFase3: ({ commit }, { fechaFinFase3 }) => {
      commit('SET_FECHAFINFASE3', fechaFinFase3);
    },
    fechaFinFase4: ({ commit }, { fechaFinFase4 }) => {
      commit('SET_FECHAFINFASE4', fechaFinFase4);
    },
    usuarios: ({ commit }, { usuarios }) => {
      commit('SET_USUARIOS', usuarios);
    },
    logout: ({ commit }) => {
      commit('RESET', '');
    }
  },
  plugins: [createPersistedState()],
})
