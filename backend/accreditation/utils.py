from accreditation import settings
from apps.user.models import *
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str,smart_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

def create_nested(obj, validated_data, field_nested, get_or_create_submodel):
    '''función para crear un registro con datos anidados'''
    fields_nested = validated_data.pop(field_nested, None)
    instance = obj.Meta.model.objects.create(**validated_data)
    if fields_nested==None: return instance
    for fields in fields_nested: get_or_create_submodel(instance, **fields)
    return instance

def update_nested(obj, instance, validated_data, field_nested, get_or_create_submodel):
    '''función para actualizar un registro con datos anidados'''
    fields_nested = validated_data.pop(field_nested, None)
    instance = super(obj.__class__, obj).update(instance, validated_data)
    if fields_nested==None: return instance
    instances_new = []
    instances_old = getattr(instance, field_nested).all()
    for fields in fields_nested: instances_new.append(get_or_create_submodel(instance, **fields))
    for ins in instances_old: ins.delete() if not ins in instances_new else False
    return instance

def remove_fields(condition, fields, *fields_remove):
    '''Función para eliminar campos según una condición'''
    if condition: [fields.pop(field, None) for field in fields_remove]

def create_nested2(obj, validated_data, *list_args):
    '''función para crear un registro con datos anidados'''
    list_args_data = [(validated_data.pop(args[0], None),) + args[1:] for args in list_args]
    instance = super(obj.__class__,obj).create(validated_data)
    for args in list_args_data:
        if args[0]==None: continue
        for fields in args[0]: args[1](instance, **fields)
    return instance

def update_nested2(obj, instance, validated_data, delete, *list_args):
    '''función para actualizar un registro con datos anidados'''
    list_args_data = [(validated_data.pop(args[0], None),) + args for args in list_args]
    instance = super(obj.__class__, obj).update(instance, validated_data)
    for args_data in list_args_data:
        if args_data[0]==None: continue
        instances_new = []
        instances_old = getattr(instance, args_data[1]).all()
        for fields in args_data[0]:
            fields_extra = fields.pop(args_data[3], None)
            sub_instance = args_data[2](instance, **fields)
            if fields_extra!=None:
                setattr(sub_instance,args_data[3],fields_extra)
                sub_instance.save()
            instances_new.append(sub_instance)
        if delete:
            for ins in instances_old: ins.delete() if not ins in instances_new else False
    return instance

def encode_user_password_reset(email):
    '''Función para codificar las credenciales para cambiar contraseña de un usuario +envio de correo'''
    try:
        user = User.objects.get(email=email) #usuario que restablecerá su contraseña
        uidb64 = urlsafe_base64_encode(smart_bytes(user.id)) #codificación del id del usuario
        token = PasswordResetTokenGenerator().make_token(user) #token para validar al usuario
        User.send_email(
            'Recuperar contraseña en el Sistema de Acreditación', #titulo del mensaje
            f'Hola {user.first_name}<br>\
            Use el siguiente link para recuperar su contraseña para el Sistema de Acreditación de la UNSA:<br>\
            http://{settings.DOMAIN}/#/ResetPassword/{uidb64}/{token}', #cuerpo del mensaje
            user.email #email de destino
        )
        return user
    except: return None

def decode_user_password_reset(uidb64,token):
    '''Función para decodificar las credenciales para cambiar contraseña de un usuario'''
    try:
        # id = force_str(urlsafe_base64_decode(uidb64))
        id = smart_str(urlsafe_base64_decode(uidb64)) #para el metodo get
        user = User.objects.get(id=id)
        if PasswordResetTokenGenerator().check_token(user,token): return user
        else: return None
    except: return None