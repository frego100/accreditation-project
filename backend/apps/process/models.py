from apps.user.models import *
from django.core.validators import RegexValidator
from django.db import models

class Process(models.Model):
    '''Modelo de los Procesos en la base de datos'''
    id_owner = models.ForeignKey(User,on_delete=models.PROTECT,null=False,blank=True,related_name='process_created',verbose_name='creado por')
    # id del usuario creador del proceso
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=False,blank=True,related_name='process_managed',verbose_name='responsable')
    # id del usuario responsable del proceso
    name = models.CharField(unique=True,blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,30}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 30 caracteres'
    )]) # nombre del proceso
    description = models.TextField(blank=True,max_length=100,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # descripción del proceso
    started = models.DateTimeField(null=True,blank=True,verbose_name='fecha de inicio')
    # fecha de inicio del proceso
    closed = models.DateTimeField(null=True,blank=True,verbose_name='fecha de cierre')
    # fecha de cierre del proceso
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del proceso
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación del registro (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización del registro (automático)

    class Meta:
        verbose_name = 'proceso'
        verbose_name_plural = 'procesos'

    def __str__(self): return f'{self.name}'

class Phase(models.Model):
    '''Modelo de las Fase de un Proceso en la base de datos'''
    id_process = models.ForeignKey(Process,on_delete=models.CASCADE,null=False,blank=False,verbose_name='proceso')
    # id del proceso al que pertenece la fase
    name = models.CharField(blank=False,max_length=20,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,20}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 20 caracteres'
    )]) # nombre de la fase
    description = models.TextField(blank=True,max_length=100,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # descripción de la fase
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado de la fase
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación del registro (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización del registro (automático)

    class Meta:
        verbose_name = 'fase'
        verbose_name_plural = 'fases'
        unique_together = ['id_process', 'name']

    def __str__(self): return f'{self.name} | {self.id_process.name}'

class Criteria(models.Model):
    '''Modelo de los Criterios de un Fase en la base de datos'''
    id_phase = models.ForeignKey(Phase,on_delete=models.CASCADE,null=False,blank=False,verbose_name='fase')
    # id de la fase al que pertenece el criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,verbose_name='responsable')
    # id del usuario responsable del criterio
    name = models.CharField(blank=False,max_length=30,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,30}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 20 caracteres'
    )]) # nombre del criterio
    description = models.TextField(blank=True,max_length=100,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # descripción del criterio
    status = models.BooleanField(default=True,verbose_name='estado')
    # estado del criterio
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación del registro (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización del registro (automático)

    class Meta:
        verbose_name = 'criterio'
        verbose_name_plural = 'criterios'
        unique_together = ['id_phase', 'name']

    def __str__(self): return f'{self.name} | {self.id_phase}'

class Indicator(models.Model):
    '''Modelo de Indicadores de un Criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio')
    # id del criterio al que pertenece el indicador
    name = models.CharField(max_length=32,verbose_name='nombre'
    ) # nombre del indicador
    description = models.TextField(max_length=100,verbose_name='descripción')
    # descripción del indicador
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del indicador
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación del registro (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización del registro (automático)

    users = models.ManyToManyField(User,blank=True,verbose_name='responsables')
    # relación de id's de los responsables de algún indicador

    class Meta:
        verbose_name = 'indicador'
        verbose_name_plural = 'indicadores'
        unique_together = ['id_criteria', 'name']

    def __str__(self): return f'{self.name}'

class Task(models.Model):
    '''Modelo de Tareas de un Indicador en la base de datos'''
    id_indicator = models.ForeignKey(Indicator,on_delete=models.CASCADE,null=False,blank=False,verbose_name='indicador')
    # id del indicador al que pertenece la tarea
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,verbose_name='responsable')
    # id del usuario responsable de la tarea
    name = models.CharField(max_length=32,verbose_name='nombre')
    # nombre de la tarea
    description = models.TextField(max_length=100,verbose_name='descripción')
    # descripción de la tarea
    started = models.DateTimeField(null=True,blank=True,verbose_name='fecha de inicio')
    # fecha de inicio de la tarea
    closed = models.DateTimeField(null=True,blank=True,verbose_name='fecha de cierre')
    # fecha de cierre de la tarea
    finished = models.DateTimeField(null=True,blank=True,verbose_name='fecha limite')
    # fecha limite de la tarea
    status = models.BooleanField(default=True,verbose_name='estado')
    # estado de la tarea
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación del registro (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización del registro (automático)

    class Meta:
        verbose_name = 'tarea'
        verbose_name_plural = 'tareas'
        unique_together = ['id_indicator', 'name']

    def __str__(self): return f'{self.name}'